package com.example.myapplication.print;

import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.databinding.ActivityAutoPrintChequeBinding;
import com.example.myapplication.databinding.ActivityPrintChequeBinding;
import com.example.myapplication.print.bill.Bill;
import com.example.myapplication.print.bill.BillConfig;
import com.example.myapplication.print.bill.print.PrinterConfig;
import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.collections4.CollectionUtils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/1/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class AutoPrintChequeActivity extends AppCompatActivity {


    private static final int BLUETOOTH_REQUEST_CODE = 12;
    private static final int BLUETOOTH_ADD_REQUEST_CODE = 11;
    ActivityAutoPrintChequeBinding mBinding;
    private PrintChequeViewModel viewModel;
    private int language = BillConfig.LANG_EN;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();

        initObservables();


    }


    private void initialize() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_auto_print_cheque);
        mBinding.setLifecycleOwner(this);
        viewModel = new ViewModelProvider(this).get(PrintChequeViewModel.class);
        mBinding.setPrintViewModel(viewModel);


        mBinding.enableBluetoothAction.setOnClickListener(this::enableBluetoothActionClicked);
        mBinding.retryAction.setOnClickListener(v -> printBill(viewModel.getPrinterName(viewModel.getCurrentPrinter())));
        mBinding.changePrinterAction.setOnClickListener(v -> changePrinterDialog());
        Glide
                .with(this)
                .asGif()
                .load(R.mipmap.ic_printing)
                .into(mBinding.imageView);
    }

    private void changePrinterDialog() {
        final String[] selectedPrinter = {viewModel.getPrinterName(viewModel.getCurrentPrinter())};

        List<String> names = viewModel.getPrintersNames().getValue();
        String[] printers = new String[names == null ? 0 : names.size()];
        if (names != null)
            names.toArray(printers);
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("CHANGE PRINTER");
        mBuilder.setSingleChoiceItems(printers, -1, (dialogInterface, index) -> {
            selectedPrinter[0] = printers[index];
        });
        DialogInterface.OnClickListener dialogActionListener = (dialog, which) -> {

            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    // store to app preference
                    break;

            }

            printBill(selectedPrinter[0]);
            dialog.dismiss();
        };

        mBuilder.setPositiveButton(R.string.one_time_use, dialogActionListener);
        mBuilder.setNegativeButton(R.string.set_as_default, dialogActionListener);

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();


    }


    private void initObservables() {
        viewModel.initBluetoothReceiver();
        viewModel.checkBluetoothStatus();

        viewModel.getBluetoothStatus().observe(this, bluetoothStatus -> {
            if (bluetoothStatus == null) return;


            switch (bluetoothStatus) {
                case ENABLED:
                    viewModel.loadPairedDevices();
                    break;
            }
        });

        viewModel.getPrintersNames().observe(this, names -> {
            printBill("HM-E30");
        });


        viewModel.getErrorMessage().observe(this, message -> {
            if (TextUtils.isEmpty(message)) return;
//            Snackbar.make(mBinding.getRoot(), message, Snackbar.LENGTH_LONG).show();
        });
    }


    public void printBill(String printer) {
        viewModel.setSelectedPrinter(printer);
//        Bitmap barcode = BillConfig.instance(Bill.createSamples(), language).createBarcode();
        String[] billSplits = BillConfig.instance(Bill.createSamples(), language).createBill();
        viewModel.print(1, null, billSplits);
    }


    public void addPrinterActionClicked(View view) {
        Intent intentBluetooth = new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivityForResult(intentBluetooth, BLUETOOTH_ADD_REQUEST_CODE);
    }

    public void enableBluetoothActionClicked(View view) {
        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBluetooth, BLUETOOTH_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case BLUETOOTH_REQUEST_CODE:
                if (resultCode == RESULT_OK) {

                    viewModel.checkBluetoothStatus();
                }

                break;

            case BLUETOOTH_ADD_REQUEST_CODE:
                viewModel.loadPairedDevices();
                break;
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        checkPrinters();
    }

    private void checkPrinters() {
        if (viewModel != null && PrinterConfig.BluetoothStatus.ENABLED.equals(viewModel.getBluetoothStatus().getValue())) {
            viewModel.loadPairedDevices();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (viewModel != null)
            viewModel.unbind();
    }
}

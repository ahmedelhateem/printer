package com.example.myapplication.print;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.R;
import com.example.myapplication.databinding.ActivityPrintChequeBinding;
import com.example.myapplication.print.bill.Bill;
import com.example.myapplication.print.bill.BillConfig;
import com.example.myapplication.print.bill.print.PrinterConfig;
import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.collections4.CollectionUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/1/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class PrintChequeActivity extends AppCompatActivity {


    private static final int BLUETOOTH_REQUEST_CODE = 12;
    private static final int BLUETOOTH_ADD_REQUEST_CODE = 11;
    ActivityPrintChequeBinding mBinding;
    Bitmap bitmap;
    private PrintChequeViewModel viewModel;
    private ArrayAdapter<String> printersAdapter;
    private List<Bitmap> loadedBitmaps;
    private String[] billSplits;
    private int language = BillConfig.LANG_EN;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();

        initLanguageSpinner();
        initPairedPrintersSpinner();
        initObservables();


    }


    private void initialize() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_print_cheque);
        mBinding.setLifecycleOwner(this);
        viewModel = new ViewModelProvider(this).get(PrintChequeViewModel.class);
        mBinding.setPrintViewModel(viewModel);


        mBinding.enableBluetoothAction.setOnClickListener(this::enableBluetoothActionClicked);
    }


    private void initLanguageSpinner() {

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_item,
                        viewModel.getLanguages());
        //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        mBinding.languageSpinner.setAdapter(spinnerArrayAdapter);
        mBinding.languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                language = position == 0 ? BillConfig.LANG_EN : BillConfig.LANG_AR;
                inflateText();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void inflateText() {
        bitmap = BillConfig.instance(Bill.createSamples(), language).createCanvas(new BillConfig.OnLoadBill() {
            @Override
            public void onBitmapLoaded(List<Bitmap> bitmaps) {
                loadedBitmaps = bitmaps;
            }

            @Override
            public void onLoadStringBill(String bill) {

            }
        });

        mBinding.imageView.setImageBitmap(bitmap);


        billSplits = BillConfig.instance(Bill.createSamples(), language).createBill();
        StringBuilder builder = new StringBuilder();
        for (String st : billSplits) {
            builder.append(st);
        }
    }


    private void initPairedPrintersSpinner() {

        printersAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_item,
                        new LinkedList<>());
        //selected item will look like a spinner set from XML
        printersAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        mBinding.printersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!CollectionUtils.isEmpty(viewModel.getPrinters())) {
                    viewModel.getPrinterSelected().setValue(true);
                    viewModel.setSelectedPrinter(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                viewModel.setSelectedPrinter(-1);

            }
        });

        mBinding.printersSpinner.setAdapter(printersAdapter);
    }


    private void initObservables() {
        viewModel.initBluetoothReceiver();
        viewModel.checkBluetoothStatus();

        viewModel.getBluetoothStatus().observe(this, bluetoothStatus -> {
            if (bluetoothStatus == null) return;


            switch (bluetoothStatus) {
                case ENABLED:
                    viewModel.loadPairedDevices();
                    break;
            }
        });

        viewModel.getPrinterSelected().observe(this, selected -> {
            if (selected != null && selected) {
                mBinding.printFab.show();
            } else
                mBinding.printFab.hide();
        });

        viewModel.getPrintersNames().observe(this, names -> {
            if (printersAdapter != null && !CollectionUtils.isEmpty(names)) {

                printersAdapter.clear();
                printersAdapter.addAll(names);

            }
        });

        viewModel.getErrorMessage().observe(this, message -> {
            if (TextUtils.isEmpty(message)) return;
            Snackbar.make(mBinding.getRoot(), message, Snackbar.LENGTH_LONG).show();
        });
    }


    public void printActionClicked(View view) {
        Bitmap barcode = BillConfig.instance(Bill.createSamples(), language).createBarcode();

        viewModel.print(1, null, billSplits);
    }


    public void addPrinterActionClicked(View view) {
        Intent intentBluetooth = new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivityForResult(intentBluetooth, BLUETOOTH_ADD_REQUEST_CODE);
    }

    public void enableBluetoothActionClicked(View view) {
        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBluetooth, BLUETOOTH_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case BLUETOOTH_REQUEST_CODE:
                if (resultCode == RESULT_OK) {

                    viewModel.checkBluetoothStatus();
                }

                break;

            case BLUETOOTH_ADD_REQUEST_CODE:
                viewModel.loadPairedDevices();
                break;
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        checkPrinters();
    }

    private void checkPrinters() {
        if (viewModel != null && PrinterConfig.BluetoothStatus.ENABLED.equals(viewModel.getBluetoothStatus().getValue())) {
            viewModel.loadPairedDevices();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (viewModel != null)
            viewModel.unbind();
    }
}

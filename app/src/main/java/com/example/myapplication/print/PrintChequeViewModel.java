package com.example.myapplication.print;

import android.annotation.SuppressLint;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.R;
import com.example.myapplication.print.bill.print.PrinterConfig;

import org.apache.commons.collections4.CollectionUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/1/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */

@SuppressLint("CheckResult")
public class PrintChequeViewModel extends AndroidViewModel {

    private static final Integer[] copies = new Integer[]{1, 2, 3, 4, 5};
    private static final String[] languages = new String[]{"EN", "AR"};


    private MutableLiveData<List<String>> printersNames = new MutableLiveData<>();
    private List<BluetoothDevice> printers = new LinkedList<>();
    private MutableLiveData<PrinterConfig.BluetoothStatus> bluetoothStatus = new MutableLiveData<>();
    private MutableLiveData<Boolean> printerVisibility = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> printerSupportedVisibility = new MutableLiveData<>(false);
    private MutableLiveData<String> printerErrorMessage = new MutableLiveData<>();
    private MutableLiveData<Boolean> addPrinterVisibility = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> printerSelected = new MutableLiveData<>(false);
    private MutableLiveData<String> printerSelectedName = new MutableLiveData<>("");
    private MutableLiveData<String> printingMessage = new MutableLiveData<>(getApplication().getString(R.string.printing_cheque));
    private MutableLiveData<Integer> printingMessageColorRes = new MutableLiveData<>(R.color.printingMessageNormal);
    private BroadcastReceiver bluetoothReceiver;
    private BluetoothDevice currentPrinter;
    private MutableLiveData<String> errorMessage = new MutableLiveData<>();



    public PrintChequeViewModel(@NonNull Application application) {
        super(application);
    }

    public Integer[] getCopies() {
        return copies;
    }

    public String[] getLanguages() {
        return languages;
    }

    public MutableLiveData<Boolean> getPrinterSupportedVisibility() {
        return printerSupportedVisibility;
    }

    public MutableLiveData<Integer> getPrintingMessageColorRes() {
        return printingMessageColorRes;
    }

    public MutableLiveData<String> getPrinterSelectedName() {
        return printerSelectedName;
    }

    public MutableLiveData<String> getPrintingMessage() {
        return printingMessage;
    }

    public MutableLiveData<Boolean> getAddPrinterVisibility() {
        return addPrinterVisibility;
    }

    public MutableLiveData<Boolean> getPrinterVisibility() {
        return printerVisibility;
    }

    public MutableLiveData<String> getPrinterErrorMessage() {
        return printerErrorMessage;
    }

    public MutableLiveData<Boolean> getPrinterSelected() {
        return printerSelected;
    }

    public MutableLiveData<String> getErrorMessage() {
        return errorMessage;
    }

    public MutableLiveData<PrinterConfig.BluetoothStatus> getBluetoothStatus() {
        return bluetoothStatus;
    }

    public List<BluetoothDevice> getPrinters() {
        return printers;
    }

    public void setPrinters(List<BluetoothDevice> printers) {
        this.printers = printers;
    }

    public MutableLiveData<List<String>> getPrintersNames() {
        return printersNames;
    }

    public BluetoothDevice getCurrentPrinter() {
        return currentPrinter;
    }

    public void print(int copies, Bitmap barcode, String... bills) {
        if (currentPrinter == null) return;


        Observable.fromCallable(() ->
                PrinterConfig.instance()
                        .connect(currentPrinter)
        )
//                .delay(2 , TimeUnit.SECONDS)
                .flatMap(aBoolean -> aBoolean != null && aBoolean
                        ? PrinterConfig.instance()
                        .write(copies, barcode, bills)
                        : Observable.error(new Throwable("Cannot connect to printer"))
                ).singleOrError()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((aBoolean, throwable) -> {
                    getPrinterSelected().setValue(true);
                    PrinterConfig.instance()
                            .close();
                    getPrintingMessage().setValue(getApplication().getString(R.string.printing_successfully));
                    getPrintingMessageColorRes().setValue(R.color.printingMessageSuccess);

                    if (throwable != null) {
                        getPrinterSelected().setValue(false);
                        getPrintingMessage().setValue(throwable.getLocalizedMessage());
                        getPrintingMessageColorRes().setValue(R.color.printingMessageError);
                        errorMessage.setValue(throwable.getLocalizedMessage());
                    }
                });


    }


    public void loadPairedDevices() {

        Set<BluetoothDevice> devices = PrinterConfig.instance().getBluetoothDevices();
        if (CollectionUtils.isEmpty(devices)) return;

        Observable.fromIterable(devices)
                .filter((device) -> PrinterConfig.isPrinterByMacAddress(device) || PrinterConfig.isPrinterByMaskAddress(device))
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((bluetoothDevices, throwable) -> {
                    getAddPrinterVisibility().setValue(CollectionUtils.isEmpty(bluetoothDevices));
                    setPrinters(bluetoothDevices);
                    List<String> names = new LinkedList<>();
                    if (!CollectionUtils.isEmpty(bluetoothDevices)) {
                        for (BluetoothDevice bluetoothDevice : bluetoothDevices) {
                            names.add(bluetoothDevice.getName());
                        }
                    }
                    getPrintersNames().setValue(names);
                });

    }

    public void checkBluetoothStatus() {
        PrinterConfig.BluetoothStatus status = PrinterConfig.instance().getBluetoothStatus();
        getBluetoothStatus().setValue(status);
        if (status == null) return;
        switch (status) {
            case ENABLED:
                getPrinterSupportedVisibility().setValue(true);
                getPrinterVisibility().setValue(true);
                break;
            case DISABLED:
                getPrinterErrorMessage().setValue("Bluetooth disabled");
                getPrinterSupportedVisibility().setValue(true);
                getPrinterVisibility().setValue(false);
                break;
            case NOT_SUPPORTED:
                getPrinterErrorMessage().setValue("Your device not supported bluetooth");
                getPrinterSupportedVisibility().setValue(false);
                getPrinterVisibility().setValue(false);
                break;
        }
    }


    public void unbind() {
        if (bluetoothReceiver != null) {
            getApplication().unregisterReceiver(bluetoothReceiver);
            bluetoothReceiver = null;
        }
    }


    public void initBluetoothReceiver() {

        if (bluetoothReceiver == null) {
            bluetoothReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    final String action = intent.getAction();
                    if (action != null && action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                        checkBluetoothStatus();
//                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
//                switch(state) {
//                    case BluetoothAdapter.STATE_OFF:
//                    case BluetoothAdapter.STATE_TURNING_OFF:
//                    case BluetoothAdapter.STATE_ON:
//                    case BluetoothAdapter.STATE_TURNING_ON:
//                }
                    }
                }
            };
        }

        IntentFilter bluetoothIntentFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        getApplication().registerReceiver(bluetoothReceiver, bluetoothIntentFilter);

    }


    public void setSelectedPrinter(String name) {
        getPrinterSelectedName().setValue(name);
        if (!CollectionUtils.isEmpty(printers)) {
            for (BluetoothDevice device : printers) {
                if (name.equals(device.getName()) || name.equals(device.getAddress())) {
                    currentPrinter = device;
                    getPrinterSelected().setValue(true);
                    getPrintingMessage().setValue(getApplication().getString(R.string.printing_cheque));
                    getPrintingMessageColorRes().setValue(R.color.printingMessageNormal);
                    return;
                }
            }

        }
        currentPrinter = null;
        getPrintingMessage().setValue(getApplication().getString(R.string.printer_not_found));
        getPrintingMessageColorRes().setValue(R.color.printingMessageError);
        getPrinterSelected().setValue(false);


    }

    public void setSelectedPrinter(int position) {

        if (!CollectionUtils.isEmpty(printers)
                && position >= 0 && position < printers.size()) {
            currentPrinter = printers.get(position);
            return;
        }

        currentPrinter = null;

    }

    public String getPrinterName(BluetoothDevice device) {
        return device == null ? "NO_NAME" : !TextUtils.isEmpty(device.getName())
                ? device.getName()
                : (!TextUtils.isEmpty(device.getAddress()) ? device.getAddress() : "NO_NAME");
    }
}

package com.example.myapplication.print.bill;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/4/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class Bill implements Serializable {

    private String companyName;
    private String branchName;
    private String taxNumber;
    private String sectionName;
    private String orderNumber;
    private String cashierName;
    private String customerCode;
    private String customerName;
    private String customerPhone;
    private List<BillItem> billItems;
    private double totalBeforeDiscount;
    private double discount;
    private double netTotal;
    private double paid;
    private double remain;
    private double vat;
    private String date;
    private String businessDate;
    private List<BillPayment> billPayments;
    private String branchPhone;
    private String orderReferenceNumber;

    public static Bill createSamples() {
        Bill bill = new Bill();
        bill.setCompanyName("الشركة المصرية للبلاستيك");
        bill.setBranchName("فرع مدينة نصر");
        bill.setTaxNumber("1256415641161");
        bill.setSectionName("سفري");
        bill.setOrderNumber("45");
        bill.setCashierName("Mohamed Hussein");
        bill.setCustomerCode("1201210210");
        bill.setCustomerName("Mohamed Albially");
        bill.setCustomerPhone("01027138040");
        bill.setBillItems(BillItem.createSamples());
        bill.setTotalBeforeDiscount(200.45);
        bill.setDiscount(9.36);
        bill.setNetTotal(193.2);
        bill.setPaid(260);
        bill.setRemain(66.5);
        bill.setVat(3.46);
        bill.setDate("4-12-2019 10:55 AM");
        bill.setBusinessDate("4-12-2019");
        bill.setBillPayments(BillPayment.createSamples());
        bill.setBranchPhone("05046546416");
        bill.setOrderReferenceNumber("SD12541341");

        return bill;

    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public List<BillItem> getBillItems() {
        return billItems;
    }

    public void setBillItems(List<BillItem> billItems) {
        this.billItems = billItems;
    }

    public double getTotalBeforeDiscount() {
        return totalBeforeDiscount;
    }

    public void setTotalBeforeDiscount(double totalBeforeDiscount) {
        this.totalBeforeDiscount = totalBeforeDiscount;
    }


    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }


    public double getNetTotal() {
        return netTotal;
    }

    public void setNetTotal(double netTotal) {
        this.netTotal = netTotal;
    }


    public String getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(String businessDate) {
        this.businessDate = businessDate;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }


    public double getRemain() {
        return remain;
    }

    public void setRemain(double remain) {
        this.remain = remain;
    }


    public double getVat() {
        return vat;
    }

    public void setVat(double vat) {
        this.vat = vat;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<BillPayment> getBillPayments() {
        return billPayments;
    }

    public void setBillPayments(List<BillPayment> billPayments) {
        this.billPayments = billPayments;
    }

    public String getBranchPhone() {
        return branchPhone;
    }

    public void setBranchPhone(String branchPhone) {
        this.branchPhone = branchPhone;
    }

    public String getOrderReferenceNumber() {
        return orderReferenceNumber;
    }

    public void setOrderReferenceNumber(String orderReferenceNumber) {
        this.orderReferenceNumber = orderReferenceNumber;
    }
}

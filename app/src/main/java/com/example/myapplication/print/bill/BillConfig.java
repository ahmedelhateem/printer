package com.example.myapplication.print.bill;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;

import androidx.annotation.ColorInt;
import androidx.annotation.IntDef;

import com.example.myapplication.print.bill.print.FormatCode;
import com.example.myapplication.print.bill.tables.Block;
import com.example.myapplication.print.bill.tables.Board;
import com.example.myapplication.print.bill.tables.Table;
import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.apache.commons.collections4.CollectionUtils;
import org.intellij.lang.annotations.Language;

import java.io.UnsupportedEncodingException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/2/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class BillConfig {


    public static final int LANG_AR = 1;
    public static final int LANG_EN = 2;
    private static final DecimalFormat decimalFormat = new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US));
    private static final int receiptWidth = 570;
    private static final int barcodeHeight = 100;
    private static final int orderTopAndBottomMargins = 100;
    private static final int orderLeftAndRightMargins = 30;
    private static final int textInsidePadding = 20;
    private static final int maxPrintingLines = 20;
    StaticLayout halfLineLayout = LayoutCreator.creator()
            .setAlignment(Layout.Alignment.ALIGN_CENTER)
            .setReceiptWidth(receiptWidth - textInsidePadding)
            .setText("----------------------------")
            .setTextColor(Color.BLACK)
            .setTextSize(30f)
            .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
            .build();
    @LanguageMode
    private int language = LANG_EN;
    private Bill bill;
    private List<StaticView> staticViews = new LinkedList<>();

    private BillConfig(Bill bill, @LanguageMode int language) {

        this.bill = bill;
        this.language = language;
    }

    public static BillConfig instance(Bill bill, @LanguageMode int language) {
        return new BillConfig(bill, language);
    }

    public Bitmap createCanvas(OnLoadBill onLoadBill) {

        String cashierName = (language == LANG_AR ? "اسم البائع : " : "Cashier Name : ").concat(bill.getCashierName());
        String customerCode = (language == LANG_AR ? "رقم العميل : " : "Customer Code : ").concat(bill.getCustomerCode());
        String customerName = (language == LANG_AR ? "اسم العميل : " : "Customer Name : ").concat(bill.getCustomerName());
        String customerPhone = (language == LANG_AR ? "هاتف العميل : " : "Customer Phone : ").concat(bill.getCustomerPhone());
        String taxNumber = (language == LANG_AR ? "الرقم الضريبي : " : "Tax Number : ").concat(bill.getTaxNumber());


        /**   create Company Name   **/
        StaticLayout companyLayout = LayoutCreator.creator()
                .setAlignment(Layout.Alignment.ALIGN_CENTER)
                .setReceiptWidth(receiptWidth - textInsidePadding)
                .setText(bill.getCompanyName())
                .setTextColor(Color.BLACK)
                .setTextSize(25f)
                .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                .build();


        staticViews.add(new StaticView(Collections.singletonList(companyLayout), true, Color.WHITE));

        /**   Print Branch Name   **/
        StaticLayout branchLayout = LayoutCreator.creator()
                .setAlignment(Layout.Alignment.ALIGN_CENTER)
                .setReceiptWidth(receiptWidth - textInsidePadding)
                .setText(bill.getBranchName())
                .setTextColor(Color.BLACK)
                .setTextSize(20f)
                .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                .build();

        staticViews.add(new StaticView(Collections.singletonList(branchLayout), true, Color.WHITE));


        boolean cont = true;
        if (cont) {
            /**   Print tax layout **/
            StaticLayout taxLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(taxNumber)
                    .setTextColor(Color.BLACK)
                    .setTextSize(20f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(taxLayout), true, Color.WHITE));


            /**   Print section Name   **/
            StaticLayout sectionLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(bill.getSectionName())
                    .setTextColor(Color.BLACK)
                    .setTextSize(22f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(sectionLayout), true, Color.WHITE));


            /**   Print order number sequence   **/
            StaticLayout orderNumberLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(bill.getOrderNumber())
                    .setTextColor(Color.WHITE)
                    .setTextSize(50f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(orderNumberLayout), true, Color.BLACK));


            /**    create info section **/

            List<StaticLayout> staticLayouts = new LinkedList<>();

            /**   Print cashier name   **/
            StaticLayout cashierNameLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(cashierName)
                    .setTextColor(Color.BLACK)
                    .setTextSize(20f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticLayouts.add(cashierNameLayout);

            /**   Print customer code   **/
            StaticLayout customerCodeLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(customerCode)
                    .setTextColor(Color.BLACK)
                    .setTextSize(20f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticLayouts.add(customerCodeLayout);

            /**   Print customer name   **/
            StaticLayout customerNameLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(customerName)
                    .setTextColor(Color.BLACK)
                    .setTextSize(20f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticLayouts.add(customerNameLayout);

            /**   Print customer phone   **/
            StaticLayout customerPhoneLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(customerPhone)
                    .setTextColor(Color.BLACK)
                    .setTextSize(20f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticLayouts.add(customerPhoneLayout);
            staticViews.add(new StaticView(staticLayouts, true, Color.WHITE));


            /**    Create items layout  **/

            int itemNameWidth = receiptWidth * 4 / 6;
            int itemQtyAndTotalWidth = receiptWidth / 6;


            // header

            staticLayouts = new LinkedList<>();

            /**   Print item Title   **/
            StaticLayout itemNameLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setReceiptWidth(itemNameWidth - textInsidePadding)
                    .setText(language == LANG_AR ? "الصنف" : "ITEMS")
                    .setTextColor(Color.BLACK)
                    .setTextSize(23f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticLayouts.add(itemNameLayout);


            /**   Print QTY title   **/
            StaticLayout itemQtyLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(itemQtyAndTotalWidth - textInsidePadding)
                    .setText(language == LANG_AR ? "الكمية" : "QTY")
                    .setTextColor(Color.BLACK)
                    .setTextSize(23f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticLayouts.add(itemQtyLayout);


            /**   Print Total title  **/
            StaticLayout itemTotalLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(itemQtyAndTotalWidth - textInsidePadding)
                    .setText(language == LANG_AR ? "الاجمالي" : "TOTAL")
                    .setTextColor(Color.BLACK)
                    .setTextSize(23f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticLayouts.add(itemTotalLayout);


            if (language == LANG_AR)
                Collections.reverse(staticLayouts);
            staticViews.add(new StaticView(staticLayouts, true, Color.parseColor("#394B4A4A"), StaticView.HORIZONTAL));

            /// items

            List<BillItem> billItems = bill.getBillItems();

            if (!CollectionUtils.isEmpty(billItems)) {
                for (BillItem billItem : billItems) {


                    staticLayouts = new LinkedList<>();


                    /**   Print Item name value   **/
                    itemNameLayout = LayoutCreator.creator()
                            .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                            .setReceiptWidth(itemNameWidth - textInsidePadding)
                            .setText(billItem.getName())
                            .setTextColor(Color.BLACK)
                            .setTextSize(25f)
                            .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                            .build();
                    staticLayouts.add(itemNameLayout);




                    /**   Print QTY  value   **/
                    itemQtyLayout = LayoutCreator.creator()
                            .setAlignment(Layout.Alignment.ALIGN_CENTER)
                            .setReceiptWidth(itemQtyAndTotalWidth - textInsidePadding)
                            .setText(decimalFormat.format(billItem.getQty()))
                            .setTextColor(Color.BLACK)
                            .setTextSize(25f)
                            .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                            .build();
                    staticLayouts.add(itemQtyLayout);


                    /**   Print Total value   **/
                    itemTotalLayout = LayoutCreator.creator()
                            .setAlignment(Layout.Alignment.ALIGN_CENTER)
                            .setReceiptWidth(itemQtyAndTotalWidth - textInsidePadding)
                            .setText(decimalFormat.format(billItem.getTotal()))
                            .setTextColor(Color.BLACK)
                            .setTextSize(25f)
                            .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                            .build();
                    staticLayouts.add(itemTotalLayout);

                    if (language == LANG_AR)
                        Collections.reverse(staticLayouts);

                    staticViews.add(new StaticView(staticLayouts, false, Color.parseColor("#14726B6B"), StaticView.HORIZONTAL));


                }
            }


            /**   ADD LINE   **/
            staticViews.add(new StaticView(null, true, Color.parseColor("#394B4A4A"), StaticView.HORIZONTAL));


            String totalBeforeDiscount = (language == LANG_AR ? "الاجمالي قبل الخصم : " : "Total before discount : ").concat(decimalFormat.format(bill.getTotalBeforeDiscount()));
            String discount = (language == LANG_AR ? "الخصم : " : "Discount : ").concat(decimalFormat.format(bill.getDiscount()));
            String netValue = (language == LANG_AR ? "الصافي : " : "Net Value : ").concat(decimalFormat.format(bill.getNetTotal()));
            String paid = (language == LANG_AR ? "المدفوع : " : "Paid : ").concat(decimalFormat.format(bill.getPaid()));
            String remain = (language == LANG_AR ? "الباقي : " : "Remain : ").concat(decimalFormat.format(bill.getRemain()));
            String vat = (language == LANG_AR ? "يشمل ضريبة القيمة المضافة : " : "Include VAT : ").concat(decimalFormat.format(bill.getVat()));


            /**   Print total before discount   **/
            StaticLayout totalBeforeDiscountLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(totalBeforeDiscount)
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(totalBeforeDiscountLayout), false, Color.WHITE));


            /**   Print total before discount   **/
            StaticLayout discountLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(discount)
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(discountLayout), false, Color.WHITE));


            /**   Print separator line   **/
            staticViews.add(new StaticView(Collections.singletonList(halfLineLayout), false, Color.WHITE));


            /**   Print total   **/
            StaticLayout netTotalLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(netValue)
                    .setTextColor(Color.BLACK)
                    .setTextSize(50f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(netTotalLayout), false, Color.WHITE));


            /**   Print Paid   **/
            StaticLayout paidLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(paid)
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(paidLayout), false, Color.WHITE));


            /**   Print remain   **/
            StaticLayout changeLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(remain)
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(changeLayout), false, Color.WHITE));


            /**   Print separator line   **/
            staticViews.add(new StaticView(Collections.singletonList(halfLineLayout), false, Color.WHITE));


            /**   Print vat   **/
            StaticLayout vatLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(vat)
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(vatLayout), false, Color.WHITE));


            /**   Print Business Date   **/
            StaticLayout businessDateLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(bill.getBusinessDate())
                    .setTextColor(Color.BLACK)
                    .setTextSize(35f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(businessDateLayout), false, Color.WHITE));


            /**   Print current date   **/
            StaticLayout dateLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(bill.getDate())
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                    .build();
            staticViews.add(new StaticView(Collections.singletonList(dateLayout), false, Color.WHITE));


            /**   Print payment methods   **/
            List<BillPayment> payments = bill.getBillPayments();
            if (!CollectionUtils.isEmpty(payments)) {
                staticLayouts = new LinkedList<>();
                for (BillPayment payment : payments) {
                    StaticLayout paymentLayout = LayoutCreator.creator()
                            .setAlignment(Layout.Alignment.ALIGN_CENTER)
                            .setReceiptWidth(receiptWidth - textInsidePadding)
                            .setText(payment.getValue(decimalFormat))
                            .setTextColor(Color.BLACK)
                            .setTextSize(30f)
                            .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                            .build();
                    staticLayouts.add(paymentLayout);
                }

                staticViews.add(new StaticView(staticLayouts, true, Color.WHITE));
            }


            staticLayouts = new LinkedList<>();
            /**    Print  Thanks  **/
            StaticLayout thanksLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(language == LANG_AR ? "شكرا لزيارتكم" : "THANKS FOR VISIT")
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL))
                    .build();
            staticLayouts.add(thanksLayout);

            /**    Print  branch phone   **/
            StaticLayout branchPhoneLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(bill.getBranchPhone())
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();
            staticLayouts.add(branchPhoneLayout);

            staticViews.add(new StaticView(staticLayouts, true, Color.WHITE));


            /**    Print  ORDER reference    **/
            StaticLayout orderReferenceLayout = LayoutCreator.creator()
                    .setAlignment(Layout.Alignment.ALIGN_CENTER)
                    .setReceiptWidth(receiptWidth - textInsidePadding)
                    .setText(bill.getOrderReferenceNumber())
                    .setTextColor(Color.BLACK)
                    .setTextSize(30f)
                    .setTextTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD))
                    .build();


            staticViews.add(new StaticView(Collections.singletonList(orderReferenceLayout), false, Color.WHITE));
        }

        /**   DRAW BITMAP  **/

        int height = barcodeHeight;

        for (StaticView staticView : staticViews) {

            height += staticView.getViewHeight();


        }

        Bitmap bitmap = Bitmap.createBitmap(receiptWidth + orderLeftAndRightMargins, height + orderTopAndBottomMargins, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.save();
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPaint(paint);

        List<Bitmap> bitmapList = new LinkedList<>();
        canvas.translate(orderLeftAndRightMargins / 2, orderTopAndBottomMargins / 2);
        for (StaticView staticView : staticViews) {
            int viewHeight = staticView.getViewHeight();
            Bitmap bitmapLayout = createBitmap(staticView);
            bitmapList.add(bitmapLayout);
            canvas.drawBitmap(bitmapLayout, 0, 0, null);
            canvas.translate(0, viewHeight);

        }

        Bitmap barcode = drawBarcode(canvas);
        bitmapList.add(barcode);

        canvas.translate(0, orderTopAndBottomMargins / 2);

        canvas.restore();


        if (onLoadBill != null)
            onLoadBill.onBitmapLoaded(bitmapList);
        return bitmap;
    }

    private Bitmap createBitmap(StaticView staticView) {


        int viewHeight = staticView.getViewHeight();
        int viewWidth = staticView.getViewWidth();

        Bitmap bitmap = Bitmap.createBitmap(receiptWidth + orderLeftAndRightMargins, viewHeight + orderTopAndBottomMargins, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.save();
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPaint(paint);


        if (staticView.withBorder || staticView.fillColor != Color.WHITE) {
            drawRectangle(canvas, staticView.withBorder, staticView.fillColor, viewHeight);
        }

        if (!CollectionUtils.isEmpty(staticView.staticLayouts)) {
            if (staticView.orientation == StaticView.HORIZONTAL) {
                canvas.translate(0, textInsidePadding / 2);
            }

            int dx = 0;
            for (StaticLayout staticLayout : staticView.staticLayouts) {
                switch (staticView.orientation) {
                    case StaticView.VERTICAL:
                        canvas.translate(textInsidePadding / 2, textInsidePadding / 2);
                        staticLayout.draw(canvas);
                        canvas.translate(-textInsidePadding / 2, staticLayout.getHeight() + (textInsidePadding / 2));
                        break;
                    case StaticView.HORIZONTAL:
                        int padding = viewHeight - staticLayout.getHeight() - textInsidePadding;

                        dx += textInsidePadding / 2;
                        canvas.translate(textInsidePadding / 2, padding / 2);
                        staticLayout.draw(canvas);
                        dx += textInsidePadding / 2;
                        canvas.translate(textInsidePadding / 2, -padding / 2);
                        dx += staticLayout.getWidth();
                        canvas.translate(staticLayout.getWidth(), 0);

                        break;

                }
            }

            if (staticView.orientation == StaticView.HORIZONTAL) {
                canvas.translate(-dx, viewHeight - textInsidePadding / 2);
            }
        }
        canvas.restore();
        return bitmap;
    }

    private void drawRectangle(Canvas canvas, boolean withBorder, int fillColor, int height) {

        int borderWidth = 2;

        //border's properties
        Paint paint = new Paint();

        paint.setColor(fillColor);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0, 0, BillConfig.receiptWidth, height, paint);

        if (withBorder) {
            paint = new Paint();
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(borderWidth);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(0, 0, BillConfig.receiptWidth, height, paint);
        }

    }

    private Bitmap drawBarcode(Canvas canvas) {

        Bitmap barcode = createBarcode();

        //border's properties
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawBitmap(barcode, 0, 0, paint);

        return barcode;

    }

    public Bitmap createBarcode() {
        Bitmap barcode = null;
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            barcode = barcodeEncoder.encodeBitmap(bill.getOrderReferenceNumber(), BarcodeFormat.CODE_128, receiptWidth, barcodeHeight);
        } catch (Exception e) {
            Log.e("BARCODE", "error : ", e);
        }
        return barcode;
    }

    public String[] createBill() {


        String company = ""
                + "\n\n" + bill.getCompanyName() + "\n"
                + bill.getBranchName() + "\n"
                + bill.getTaxNumber() + "\n"
                + bill.getSectionName() + "\n"
                + " \n"
                + bill.getOrderNumber()
                + " \n";
        String cashierData = (language == LANG_AR ? "اسم البائع : " : "Cashier Name : ") + bill.getCashierName();
        List<String> itemsHeaders = language == LANG_AR ? Arrays.asList("السعر", "الكمية", "الاصناف") : Arrays.asList("ITEMS", "QTY", "PRICE");
        List<List<String>> itemsData = new LinkedList<>();
        for (BillItem billItem : bill.getBillItems()) {
            if (language == LANG_EN) {
                itemsData.add(Arrays.asList(billItem.getName(), decimalFormat.format(billItem.getQty()), decimalFormat.format(billItem.getTotal())));
            } else {
                itemsData.add(Arrays.asList(decimalFormat.format(billItem.getTotal()), decimalFormat.format(billItem.getQty()), billItem.getName()));
            }
        }

        List<Integer> itemsColumnWidths = language == LANG_EN ? Arrays.asList(25, 8, 10) : Arrays.asList(10, 8, 25);

        String summary = language == LANG_AR ?
                ""
                        + "القيمة قبل الخصم\n"
                        + "الخصم\n"
                        + "الصافي\n"
                        + "المدفوع\n"
                        + "الباقي\n"
                : ""
                + "Total before discount\n"
                + "Discount\n"
                + "Net Amount\n"
                + "Paid\n"
                + "Remain\n";
        String summaryVal = ""
                + decimalFormat.format(bill.getTotalBeforeDiscount()) + "\n"
                + decimalFormat.format(bill.getDiscount()) + "\n"
                + decimalFormat.format(bill.getNetTotal()) + "\n"
                + decimalFormat.format(bill.getPaid()) + "\n"
                + decimalFormat.format(bill.getRemain()) + "\n";
        String businessDate = ""
                + (language == LANG_AR ? "تاريخ العمل" : "Business Date") + "\n"
                + "---------------------\n"
                + bill.getBusinessDate() + "\n";
        String billDate = ""
                + (language == LANG_AR ? "تاريخ الشراء" : "Order Date") + "\n"
                + "---------------------\n"
                + bill.getDate() + "\n";
        String referenceNumber = bill.getOrderReferenceNumber();
        String billEnd = "\n\n\n--------------------------------------";
        //bookmark
        Board b = new Board(48);

        Block companyBlock = new Block(b, 46, 9, company).allowGrid(false).setBlockAlign(Block.BLOCK_CENTRE)
                .setDataAlign(Block.DATA_CENTER);
        b.setInitialBlock(companyBlock);

        Block cashierDataBlock = new Block(b, 46, 1, cashierData).setDataAlign(language == LANG_EN ? Block.DATA_MIDDLE_LEFT : Block.DATA_MIDDLE_RIGHT);
        b.getBlock(0).setBelowBlock(cashierDataBlock);
        Table table = new Table(b, 48, itemsHeaders, itemsData, itemsColumnWidths, language).setGridMode(Table.GRID_FULL);

        b.appendTableTo(1, Board.APPEND_BELOW, table);
//
        Block summaryBlock = new Block(b, 25, 6, summary).allowGrid(false).setDataAlign(Block.DATA_MIDDLE_RIGHT);
        Block summaryValBlock = new Block(b, 12, 6, summaryVal).allowGrid(false).setDataAlign(Block.DATA_MIDDLE_RIGHT);
        b.getBlock(2 + (3 * itemsData.size())).setBelowBlock(language == LANG_EN ? summaryBlock : summaryValBlock);
        if (language == LANG_EN)
            summaryBlock.setRightBlock(summaryValBlock);
        else
            summaryValBlock.setRightBlock(summaryBlock);
        Block businessDateBlock = new Block(b, 24, 4, businessDate).setDataAlign(Block.DATA_BOTTOM_MIDDLE).allowGrid(false);
        summaryBlock.setBelowBlock(businessDateBlock);
        if (language == LANG_EN)
            summaryBlock.setBelowBlock(businessDateBlock);
        else
            summaryValBlock.setBelowBlock(businessDateBlock);
        businessDateBlock.setRightBlock(new Block(b, 24, 4, billDate).setDataAlign(Block.DATA_BOTTOM_MIDDLE).allowGrid(false));
        Block referenceBlock = new Block(b, 48, 2, referenceNumber).setDataAlign(Block.DATA_BOTTOM_MIDDLE).allowGrid(false);
        businessDateBlock.setBelowBlock(referenceBlock);
        Block endBlock = new Block(b, 48, 4, billEnd).setDataAlign(Block.DATA_CENTER).allowGrid(false);
        referenceBlock.setBelowBlock(endBlock);
//        b.showBlockIndex(true);
        String tableString = b.invalidate().build().getPreview();


        System.out.println(tableString);


        return createSplits(tableString);
    }

    private String[] createSplits(String bill) {


        String[] splits = bill.split("\n");
        for (int index = 0; index < splits.length; index++) {
            splits[index] = splits[index].concat("\n");
        }
        return splits;
    }

    private int lines(String input) {
        Matcher m = Pattern.compile("\r\n|\r|\n").matcher(input);
        int lines = 1;
        while (m.find()) {
            lines++;
        }
        return lines;
    }

    @IntDef({LANG_AR, LANG_EN})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LanguageMode {
    }


    public interface OnLoadBill {
        void onBitmapLoaded(List<Bitmap> bitmaps);

        void onLoadStringBill(String bill);
    }

    private static class LayoutCreator {
        private float textSize;
        private int textColor;
        private Typeface typeface;
        private String text;
        private int receiptWidth;
        private Layout.Alignment alignment;

        private LayoutCreator() {
        }

        static LayoutCreator creator() {
            return new LayoutCreator();
        }


        LayoutCreator setTextSize(float textSize) {
            this.textSize = textSize;
            return this;
        }

        LayoutCreator setTextColor(int textColor) {
            this.textColor = textColor;
            return this;
        }

        LayoutCreator setTextTypeface(Typeface typeface) {
            this.typeface = typeface;
            return this;
        }

        LayoutCreator setText(String text) {
            this.text = text;
            return this;
        }

        LayoutCreator setReceiptWidth(int receiptWidth) {
            this.receiptWidth = receiptWidth;
            return this;
        }

        LayoutCreator setAlignment(Layout.Alignment alignment) {
            this.alignment = alignment;
            return this;
        }


        StaticLayout build() {
            TextPaint textPaint = new TextPaint();
            textPaint.setAntiAlias(true);
            textPaint.setTextSize(textSize);
            textPaint.setColor(textColor);
            textPaint.setTypeface(typeface);

            return new StaticLayout(text, textPaint
                    , receiptWidth
                    , alignment
                    , 1.0f
                    , 0
                    , false);

        }


    }

    private static class StaticView {
        static final int VERTICAL = 1;
        static final int HORIZONTAL = 2;
        private List<StaticLayout> staticLayouts;
        private boolean withBorder;
        @ColorInt
        private int fillColor;
        @OrientationMode
        private int orientation = VERTICAL;

        StaticView(List<StaticLayout> staticLayout, boolean withBorder, int fillColor) {
            this.staticLayouts = staticLayout;
            this.withBorder = withBorder;
            this.fillColor = fillColor;
        }

        StaticView(List<StaticLayout> staticLayout, boolean withBorder, int fillColor, @OrientationMode int orientation) {
            this.staticLayouts = staticLayout;
            this.withBorder = withBorder;
            this.fillColor = fillColor;
            this.orientation = orientation;
        }

        int getViewHeight() {
            int height = 0;
            if (!CollectionUtils.isEmpty(staticLayouts)) {
                for (StaticLayout staticLayout : staticLayouts) {
                    switch (orientation) {
                        case VERTICAL:
                            height += BillConfig.textInsidePadding;
                            height += staticLayout.getHeight();
                            break;
                        case HORIZONTAL:
                            if (height < staticLayout.getHeight())
                                height = staticLayout.getHeight();
                            break;
                    }
                }
                if (orientation == HORIZONTAL) {
                    height += BillConfig.textInsidePadding;
                }
            }

            return height;
        }

        int getViewWidth() {
            int width = 0;
            if (!CollectionUtils.isEmpty(staticLayouts)) {
                for (StaticLayout staticLayout : staticLayouts) {
                    width += BillConfig.textInsidePadding;
                    width += staticLayout.getWidth();
                }
            }
            return width;
        }

        @IntDef({VERTICAL, HORIZONTAL})
        @Retention(RetentionPolicy.SOURCE)
        @interface OrientationMode {
        }
    }


}

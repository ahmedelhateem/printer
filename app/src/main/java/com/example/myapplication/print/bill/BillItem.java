package com.example.myapplication.print.bill;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/4/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class BillItem implements Serializable {
    private String name;
    private double qty;
    private double total;

    public BillItem(String name, double qty, double total) {
        this.name = name;
        this.qty = qty;
        this.total = total;
    }

    public static List<BillItem> createSamples() {
        List<BillItem> items = new LinkedList<>();
        items.add(new BillItem("شاي", 1, 9));
        items.add(new BillItem("Hot chocolate ", 3, 50.5));
        items.add(new BillItem("صنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدةصنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدة", 1, 4.99));
        items.add(new BillItem("شكولاته ساخنة", 3, 50.5));
        items.add(new BillItem("قهوة تركي", 2, 32.2));
//        items.add(new BillItem("صنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدةصنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدة", 1, 4.99));
//        items.add(new BillItem("قهوة تركي", 2, 32.2));
//        items.add(new BillItem("صنف تامين", 1, 42.5));
//        items.add(new BillItem("صنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدةصنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدة", 1, 4.99));
//        items.add(new BillItem("صنف تامين", 1, 42.5));
//        items.add(new BillItem("صنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدةصنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدة", 1, 4.99));
//        items.add(new BillItem("صنف تامين", 1, 42.5));
//        items.add(new BillItem("صنف خدمة", 1, 41.1));
//        items.add(new BillItem("صنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدةصنف اسمه طويل جدا للطباعه اسمه اي كلام ع الفاضي بس كدة", 1, 4.99));
//        items.add(new BillItem("صنف خدمة", 1, 41.1));
        return items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}

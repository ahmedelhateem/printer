package com.example.myapplication.print.bill;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/4/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class BillPayment implements Serializable {

    private String method;
    private double amount;

    public BillPayment(String method, double amount) {
        this.method = method;
        this.amount = amount;
    }

    public static List<BillPayment> createSamples() {
        List<BillPayment> payments = new LinkedList<>();
        payments.add(new BillPayment("VISA", 100));
        payments.add(new BillPayment("CASH", 50));
        payments.add(new BillPayment("MASTERCARD", 70));
        return payments;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getValue(DecimalFormat decimalFormat) {
        return method.concat("   =   ").concat(decimalFormat.format(amount));
    }
}

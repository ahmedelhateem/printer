package com.example.myapplication.print.bill.print;

import java.lang.ref.WeakReference;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/12/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class FormatCode {


    public static final String ISO_8859_6 = "ISO-8859-6";
    public static final String ISO_8859_1 = "ISO_8859_1";
    public static final String UTF_8 = "UTF-8";
    public static final String UTF_16 = "UTF-16";
    public static final String WINDOWS_1256 = "windows-1256";


    public static String[] CODES = new String[]{
            ISO_8859_6
    };
}

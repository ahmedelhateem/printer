package com.example.myapplication.print.bill.print;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/2/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
public class PrinterConfig {



    private static final String TAG = PrinterConfig.class.getSimpleName();
    private static final UUID COMMON_PRINTERS_ID = UUID.fromString("0001101-0000-1000-8000-00805F9B34FB");
    private static final UUID BUILT_IN_PRINTERS_ID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String[] BUILT_IN_PRINTERS_MAC_ADDRESS = new String[]{
            "00:11:22:33:44:55"
    };
    private static final int printerMask = 0b000001000000011010000000;
    private static PrinterConfig printerConfig;
    private BluetoothSocket bluetoothSocket;

    private PrinterConfig() {

    }




    public static PrinterConfig instance() {
        if (printerConfig == null)
            printerConfig = new PrinterConfig();

        return printerConfig;
    }

    public static boolean isPrinterByMaskAddress(BluetoothDevice device) {
        if (device == null) return false;
        int fullCod = device.getBluetoothClass().hashCode();
        Log.d(TAG, "FULL COD: " + fullCod);
        Log.d(TAG, "MASK RESULT " + (fullCod & printerMask));
        return (fullCod & printerMask) == printerMask;
    }


    public static boolean isPrinterByMacAddress(BluetoothDevice device) {
        if (device == null) return false;
        for (String macAddress : BUILT_IN_PRINTERS_MAC_ADDRESS)
            if (macAddress.equals(device.getAddress()))
                return true;
        return false;
    }

    public static void resetPrint(OutputStream outputStream) {
        try {
            outputStream.write(PrinterCommands.ESC_FONT_COLOR_DEFAULT);
            outputStream.write(PrinterCommands.FS_FONT_ALIGN);
            outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
            outputStream.write(PrinterCommands.ESC_CANCEL_BOLD);
            outputStream.write(PrinterCommands.LF);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BluetoothStatus getBluetoothStatus() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            return BluetoothStatus.NOT_SUPPORTED;
        } else if (!mBluetoothAdapter.isEnabled()) {
            // Bluetooth is not enabled :)
            return BluetoothStatus.DISABLED;
        } else {
            // Bluetooth is enabled
            return BluetoothStatus.ENABLED;
        }
    }

    public Set<BluetoothDevice> getBluetoothDevices() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        return mBluetoothAdapter == null ? null : mBluetoothAdapter.getBondedDevices();

    }

    public boolean connect(BluetoothDevice device) throws IOException {
        if (bluetoothSocket == null || !bluetoothSocket.isConnected()) {
            UUID id = getPrintersUUID(device);
            if (id == null)
                return false;
            bluetoothSocket = device.createRfcommSocketToServiceRecord(id);
            bluetoothSocket.connect();
            return true;
        }
        return true;
    }

    private UUID getPrintersUUID(BluetoothDevice device) {
        if (isPrinterByMacAddress(device))
            return BUILT_IN_PRINTERS_ID;
        else if (isPrinterByMaskAddress(device))
            return COMMON_PRINTERS_ID;
        return null;
    }

    public Observable<Boolean> write(int copies, Bitmap barcode, String... outputs) throws IOException {
        if (bluetoothSocket == null || !bluetoothSocket.isConnected())
            return Observable.error(new Throwable("Printer not connected"));


        OutputStream os = bluetoothSocket.getOutputStream();


        for (int copy = 0; copy < copies; copy++) {
            for (String st : outputs) {

                printText(os, st, FormatCode.ISO_8859_6);
            }


            if (barcode != null)
                printPhoto(os, barcode);


            String billEnd = "\n\n\n--------------------------------------";

            printText(os, billEnd, FormatCode.ISO_8859_6);
        }

        os.flush();
        os.close();

        return Observable.just(true);

    }

    //print custom
    private void printCustom(OutputStream outputStream, String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B, 0x21, 0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 3- bold with large text
        try {
            switch (size) {
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align) {
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            outputStream.write(msg.getBytes());
            outputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print photo
    public void printPhoto(OutputStream outputStream, Bitmap bmp) {
        try {
            if (bmp != null) {

                Thread.sleep(100);
                byte[] printformat = new byte[]{0x1B, 0x21, 0x03};
                outputStream.write(printformat);


                outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                byte[] command = Utils.decodeBitmap(bmp);
                printText(outputStream, command);

                Thread.sleep(1500);
            } else {
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    //print unicode
    public void printUnicode(OutputStream outputStream) {
        try {
            outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
            printText(outputStream, Utils.UNICODE_TEXT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print new line
    private void printNewLine(OutputStream outputStream) {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print text
    private void printText(OutputStream outputStream, String msg, String code) {
        try {
            // Print normal text
            printText(outputStream, msg.getBytes(code));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //print byte[]
    private void printText(OutputStream outputStream, byte[] msg) {
        try {
            // Print normal text
            Thread.sleep(100);
            outputStream.write(msg);
            outputStream.write(PrinterCommands.reset);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }


    public boolean close() throws IOException {
        if (bluetoothSocket == null || !bluetoothSocket.isConnected()) return false;
        bluetoothSocket.close();
        bluetoothSocket = null;

        return true;
    }


    public enum BluetoothStatus {
        NOT_SUPPORTED, ENABLED, DISABLED
    }

}

package com.example.myapplication.print.bill.tables;

/**
 * Created by DEV HUSSEIN , dev.hussein.smartpan@gmail.com on 12/10/2019.
 * Copyright (c) 2019 , SMARTPAN All rights reserved
 */
class Charr {

    protected static final char S = ' ';

    protected static final char NL = '\n';

    protected static final char P = ' ';

    protected static final char D = '=';

    protected static final char VL = '¦';

    private final int x;

    private final int y;

    private final String c;

    protected Charr(int x, int y, String c) {
        this.x = x;
        this.y = y;
        this.c = c;
    }

    protected Charr(int x, int y, char c) {
        this.x = x;
        this.y = y;
        this.c = String.valueOf(c);
    }

    protected int getX() {
        return x;
    }

    protected int getY() {
        return y;
    }

    protected String getC() {
        return c;
    }

}
